document.addEventListener("keydown", escapeModalDelete, false);

function openModalDelete(key){
    var modal = document.getElementById('modalDelete_' + key);
    modal.style.display = "block";
}

function closeModalDelete(key){
    var modal = document.getElementById('modalDelete_' + key);
    modal.style.display = "none";
}

function closeAllModalsDelete(){
    var modals = document.getElementsByClassName('modal');

    for (let key in modals) {
        if (typeof modals[key].style !== 'undefined') {
            modals[key].style.display = "none";
        }
    }
}

function escapeModalDelete(evt){
    var keyCode = evt.keyCode;

    if(keyCode == 27){	// Escape key.
        closeAllModalsDelete();
    }
}

document.addEventListener(
    "click",
    function(event){
        if (event.target.matches(".modal")) {
            closeAllModalsDelete();
        }
    }
)
