<?php

if (file_exists(dirname(__DIR__).'/var/cache/prod/App_KernelProdContainer.preload.php')) {
    ini_set('memory_limit', '-1');

    require dirname(__DIR__).'/var/cache/prod/App_KernelProdContainer.preload.php';
}
