<?php

namespace App\Form;

use App\Entity\Transaction;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use App\Repository\TransactionTypeRepository;
use App\Repository\CategoryRepository;
use App\Entity\TransactionType;
use App\Entity\Category;

class TransactionFormType extends AbstractType
{
    private $transactionTypeRepository;
    private $categoryRepository;

    public function __construct(
        TransactionTypeRepository $transactionTypeRepository,
        CategoryRepository $categoryRepository
    )
    {
        $this->transactionTypeRepository = $transactionTypeRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $typeChoices = [];
        $categoryChoices = [];

        $types = $this->transactionTypeRepository->findAll();
        $categories = $this->categoryRepository->findAll();

        foreach ($types as $type) {
            $typeChoices[] = $type;
        }

        foreach ($categories as $category) {
            $categoryChoices[] = $category;
        }

        $builder
            ->add('type', ChoiceType::class, [
                'choices' => $typeChoices,
                'choice_value' => 'name',
                'choice_label' => function(?TransactionType $type) {
                    return $type ? strtoupper($type->getName()) : '';
                }
            ])
            ->add('amount', MoneyType::class, [
                'currency' => 'UAH',
            ])
            ->add('category', ChoiceType::class, [
                'choices' => $categoryChoices,
                'choice_value' => 'name',
                'choice_label' => function(?Category $category) {
                    return $category ? strtoupper($category->getName()) : '';
                }
            ])
            ->add('comment', TextareaType::class)
            ->add('executionDate', DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Transaction::class,
        ]);
    }
}
