<?php

namespace App\Traits;

trait FilterTrait
{
    private function addFirstZeroParameter($zeroItem, $items)
    {
        $arrayWithZeroValue = [];
        $arrayWithZeroValue[] = $zeroItem;

        foreach ($items as $item) {
            $arrayWithZeroValue[] = $item;
        }

        return $arrayWithZeroValue;
    }
}
