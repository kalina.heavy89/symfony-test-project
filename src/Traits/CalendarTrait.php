<?php

namespace App\Traits;

trait CalendarTrait
{
    private function getMonths(array $monthsNames)
    {
        $months = [];
        for ($i = 0; $i < 12; $i++) {
            $months[] = [
                'id' => ($i < 9 ? '0' : '') . (string) $i + 1,
                'name' => $monthsNames[$i],
            ];
        }

        return $months;
    }

    private function getMonthName(array $monthsNames, $monthId)
    {
        return $monthsNames[((int) $monthId) - 1];
    }

    private function getNumberOfDaysInMonth(array $monthDays, $monthId, $year)
    {
        $monthNumber = (int) $monthId - 1;

        if ($monthNumber == 1) {
            if (((int) $year - 2000) % 4 === 0 ) {
                return 29;
            }
        }

        return $monthDays[$monthNumber];
    }

    private function getYearsRange($year, $offset)
    {
        return range($year - $offset, $year + $offset);
    }

    private function getCurrentDateValues()
    {
        list($year, $month, $day) = explode("/", date('Y/m/d'));

        return [
            'year' => $year,
            'month' => $month,
            'day' => $day,
        ];
    }

    private function getDateValues(object $dateTimeObject)
    {
        $date = $dateTimeObject->format('Y-m-d');
        list($year, $month, $day) = explode("-", $date);

        return [
            'year' => $year,
            'month' => $month,
            'day' => $day,
        ];
    }
}
