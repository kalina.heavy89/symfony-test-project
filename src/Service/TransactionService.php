<?php

namespace App\Service;

use App\Repository\TransactionRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Security\Core\Security;
use App\Entity\User;
use App\Traits\CalendarTrait;

class TransactionService
{
    use CalendarTrait;

    private const ITEMS_PER_PAGE = 5;

    protected $repo;
    private $user;

    /**
    * Constructor.
    *
    * @var TransactionRepository $repo
    */
    public function __construct(TransactionRepository $repo, Security $security)
    {
        $this->repo = $repo;
        $this->user = $security->getUser();
    }

    public function getFilters(Request $request)
    {
        $search = '';
        $transactionType = 0;
        $transactionCategory = 0;

        $dateValues = $this->getCurrentDateValues();
        $year = $dateValues['year'];
        $month = $dateValues['month'];

        if ($request->query->get('search')) {
            $search = $request->query->get('search');
        }

        if ($request->query->get('type')) {
            $transactionType = $request->query->get('type');
        }

        if ($request->query->get('category')) {
            $transactionCategory = $request->query->get('category');
        }

        if ($request->query->get('month')) {
            $month = $request->query->get('month');
        }

        if ($request->query->get('year')) {
            $year = $request->query->get('year');
        }

        return [
            'search' => $search,
            'type' => $transactionType,
            'category' => $transactionCategory,
            'month' => $month,
            'year' => $year,
        ];
    }

    public function getPage(Request $request)
    {
        $page = 1;

        if ($request->query->get('page')) {
            $page = $request->query->get('page');
        }

        return $page;
    }

    public function getTransactionsPerMonth($month, $year)
    {
        return $this->repo->getTransactions($this->user, [
            'month' => $month,
            'year' => $year,
        ]);
    }

    public function getAmountOfTransactions($transactions)
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $totalAmount = 0;

        foreach ($transactions as $transaction) {
            $type = $transaction->getType();
            $propertyAccessor->setValue($transaction, 'type', $type);

            if ($transaction->getType()->getName() == 'income') {
                $totalAmount += $transaction->getAmount();
            }

            if ($transaction->getType()->getName() == 'expense') {
                $totalAmount -= $transaction->getAmount();
            }
        }

        return $totalAmount;
    }

    public function getPagesNumber($filters)
    {
        $countTransactions = $this->repo->countTransactions($this->user, $filters);
        return ceil($countTransactions / self::ITEMS_PER_PAGE);
    }

    public function getTransactions($filters, $page, $order = 'ASC')
    {
        $transactions = $this->repo->getTransactions($this->user, $filters, $order, $page);
        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        foreach ($transactions as $transaction) {
            $type = $transaction->getType();
            $category = $transaction->getCategory();

            $propertyAccessor->setValue($transaction, 'type', $type);
            $propertyAccessor->setValue($transaction, 'category', $category);
        }

        return $transactions;
    }

    public function getPreviousTransactions(User $user, $endDate): array
    {
        return $this->repo->getPreviousTransactions($user, $endDate);
    }
}
