<?php

namespace App\Service;

use App\Traits\CalendarTrait;
use App\Interfaces\CalendarDefinitions;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;
use App\Service\TransactionService;
use Symfony\Component\Security\Core\Security;

class ChartService implements CalendarDefinitions
{
    use CalendarTrait;

    private $chartBuilder;
    private $transactionService;
    private $user;

    public function __construct(
        ChartBuilderInterface $chartBuilder,
        TransactionService $transactionService,
        Security $security
    )
    {
        $this->chartBuilder = $chartBuilder;
        $this->transactionService = $transactionService;
        $this->user = $security->getUser();
    }

    public function getTransactionsChartData(array $transactions)
    {
        $days = [];
        $amounts = [];
        $i = 0;

        foreach ($transactions as $key => $transaction) {
            $transactionDate = $transaction->getExecutionDate();
            $day = $this->getDateValues($transactionDate)['day'];
            $sign = $transaction->getType()->getName() == 'expense' ? - 1 : 1;
            $amount = $transaction->getAmount();

            if ($key === 0 || $days[$i - 1] !== $day) {
                $days[$i] = $day;
                $amounts[$i] = $amount * $sign;
                $i++;
            } else if ($days[$i - 1] === $day) {
                $amounts[$i - 1] += $amount * $sign;
            }
        }

        return [
            'days' => $days,
            'amounts' => $amounts,
        ];
    }

    public function splitIntoIncomesAndExpenses($amounts)
    {
        $incomes = [];
        $expenses = [];

        foreach ($amounts as $amount) {
            if ($amount > 0) {
                $incomes[] = $amount;
                $expenses[] = 0;
            } else {
                $incomes[] = 0;
                $expenses[] = $amount;
            }
        }

        return [
            'incomes' => $incomes,
            'expenses' => $expenses,
        ];
    }

    public function createChartBar($labels, $incomes, $expenses, $title)
    {
        $chart = $this->chartBuilder->createChart(Chart::TYPE_BAR);

        $chart->setData([
            'labels' => $labels,
            'datasets' => [
                [
                    'label' => 'Incomes',
                    'backgroundColor' => 'rgb(55, 200, 50)',
                    'borderColor' => 'rgb(55, 99, 12)',
                    'data' => $incomes,
                ],
                [
                    'label' => 'Expenses',
                    'backgroundColor' => 'rgb(255, 99, 132)',
                    'borderColor' => 'rgb(150, 19, 12)',
                    'data' => $expenses,
                ],
            ],
        ]);

        $chart->setOptions([
            'responsive' => true,
            'scales' => [
                'x' => [
                    'stacked' => true,
                ],
                'y' => [
                    'stacked' => true
                ]
            ],
            'elements' => [
                'bar' => [
                    'borderWidth' => 2
                ]
            ],
            'plugins' => [
                'title' => [
                    'display' => true,
                    'text' => $title
                ]
            ],
        ]);

        return $chart;
    }

    public function getMoneyBalanceData($year, $month, $amounts, $days)
    {
        $zeroDate = $year . '-' . $month . '-01';

        $previousTransactions = $this->transactionService->getPreviousTransactions($this->user, $zeroDate);
        $previousTransactionsAmount = $this->transactionService->getAmountOfTransactions($previousTransactions);

        $moneyBalance = [];
        $moneyBalance[] = $previousTransactionsAmount;

        foreach ($amounts as $key => $amount) {
            $moneyBalance[] = $moneyBalance[$key] + $amount;
        }

        $monthDaysNumber = $this->getNumberOfDaysInMonth(self::DAYS_IN_MONTHS, $month, $year);

        if (end($days) != $monthDaysNumber) {
            $days[] = (string) $monthDaysNumber;
        }

        $moneyBalance[] = end($moneyBalance);

        return [
            'days' => $days,
            'balance' => $moneyBalance,
        ];
    }

    public function createChartLine($labels, $values, $title)
    {
        $chart = $this->chartBuilder->createChart(Chart::TYPE_LINE);

        $chart->setData([
            'labels' => $labels,
            'datasets' => [
                [
                    'backgroundColor' => 'rgb(55, 99, 12)',
                    'borderColor' => 'rgb(55, 99, 12)',
                    'data' => $values,
                    'fill'=> [
                        'above' => 'rgb(55, 200, 50)',
                        'below' => 'rgb(255, 99, 132)',
                        'target' => [
                            'value' => 0
                        ]
                    ],
                ],
            ],
        ]);

        $chart->setOptions([
            'scales' => [
                'y' => [
                    'suggestedMin' => 0,
                    'suggestedMax' => 100,
                ],
            ],
            'elements' => [
                'bar' => [
                    'borderWidth' => 2
                ]
            ],
            'plugins' => [
                'title' => [
                    'display' => true,
                    'text' => $title
                ],
                'legend' => [
                    'display' => false
                ],
            ],
        ]);

        return $chart;
    }
}
