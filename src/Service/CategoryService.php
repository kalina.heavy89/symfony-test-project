<?php

namespace App\Service;

use App\Repository\CategoryRepository;
use App\Traits\FilterTrait;

class CategoryService
{
    use FilterTrait;

    protected $repo;

    /**
    * Constructor.
    *
    * @var CategoryRepository $repo
    */
    public function __construct(CategoryRepository $repo)
    {
        $this->repo = $repo;
    }

    public function getCategoriesForSelector()
    {
        $zeroCategory = [
            'id' => 0,
            'name' => ''
        ];

        $existingCategories = $this->repo->findAll();
        return $this->addFirstZeroParameter($zeroCategory, $existingCategories);
    }
}
