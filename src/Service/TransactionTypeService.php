<?php

namespace App\Service;

use App\Repository\TransactionTypeRepository;
use App\Traits\FilterTrait;

class TransactionTypeService
{
    use FilterTrait;

    protected $repo;

    /**
    * Constructor.
    *
    * @var TransactionTypeRepository $repo
    */
    public function __construct(TransactionTypeRepository $repo)
    {
        $this->repo = $repo;
    }

    public function getTransactionTypesForSelector()
    {
        $zeroType = [
            'id' => 0,
            'name' => ''
        ];

        $existingTransactionTypes = $this->repo->findAll();
        return $this->addFirstZeroParameter($zeroType, $existingTransactionTypes);
    }
}
