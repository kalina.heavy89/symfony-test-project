<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\TransactionType;

class TransactionTypeFixtures extends Fixture
{
    private $transactionTypes = ['income', 'expense'];

    public function load(ObjectManager $manager): void
    {
        foreach ($this->transactionTypes as $type) {
            $transactionType = new TransactionType();
            $transactionType->setName($type);
            $manager->persist($transactionType);
        }

        $manager->flush();
    }
}
