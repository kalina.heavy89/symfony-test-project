<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Category;

class CategoryFixtures extends Fixture
{
    private $categories = [
        'building and repair',
        'charity',
        'clothes and shoes',
        'computers and gadgets',
        'credit',
        'deposit',
        'food',
        'hobby',
        'household appliances',
        'profit',
        'salary',
        'sport',
        'taxes',
        'transport',
        'utilities',
        'vacation',
        'win',
    ];

    public function load(ObjectManager $manager): void
    {
        foreach ($this->categories as $category) {
            $categoryInstance= new Category();
            $categoryInstance->setName($category);
            $manager->persist($categoryInstance);
        }

        $manager->flush();
    }
}
