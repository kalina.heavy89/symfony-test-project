<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use App\Entity\Transaction;
use App\Form\TransactionFormType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\TransactionRepository;
use App\Service\TransactionService;
use App\Service\TransactionTypeService;
use App\Service\CategoryService;
use App\Interfaces\CalendarDefinitions;
use App\Traits\CalendarTrait;

class TransactionController extends AbstractController implements CalendarDefinitions
{
    use CalendarTrait;

    private $security;
    private $entityManager;
    private $user;
    private $transactionRepository;
    private $transactionService;
    private $transactionTypeService;
    private $categoryService;

    public function __construct(
        Security $security,
        EntityManagerInterface $entityManager,
        TransactionRepository $transactionRepository,
        TransactionService $transactionService,
        TransactionTypeService $transactionTypeService,
        CategoryService $categoryService
    )
    {
        $this->security = $security;
        $this->entityManager = $entityManager;
        $this->user = $this->security->getUser();
        $this->transactionRepository = $transactionRepository;
        $this->transactionService = $transactionService;
        $this->transactionTypeService = $transactionTypeService;
        $this->categoryService = $categoryService;
    }

    #[Route('/', name: 'app_default')]
    public function index(): Response
    {
        if ($this->user) {
            return $this->redirectToRoute('app_transactions');
        }

        return $this->render('index.html.twig');
    }

    #[Route('/transactions/display', name: 'app_transactions')]
    public function display(Request $request): Response
    {
        $yearsOffset = 5;
        $filters = $this->transactionService->getFilters($request);

        $transactionsPerMonth = $this->transactionService->getTransactionsPerMonth(
            $filters['month'],
            $filters['year']
        );
        $totalAmount = $this->transactionService->getAmountOfTransactions($transactionsPerMonth);

        $pages = $this->transactionService->getPagesNumber($filters);
        $page = $this->transactionService->getPage($request);
        $transactions = $this->transactionService->getTransactions($filters, $page, 'DESC');
        $transactionTypes = $this->transactionTypeService->getTransactionTypesForSelector();
        $categories = $this->categoryService->getCategoriesForSelector();
        $months = $this->getMonths(self::MONTHS);
        $years = $this->getYearsRange($filters['year'], $yearsOffset);

        return $this->render('transactions/display.html.twig', [
            'user' => $this->user,
            'transactions' => $transactions,
            'total_amount' => $totalAmount,
            'pages' => $pages,
            'page' => $page,
            'types' => $transactionTypes,
            'categories' => $categories,
            'months' => $months,
            'years' => $years,
            'filters' => $filters,
        ]);
    }

    #[Route('/transactions/new', name: 'app_transaction_new')]
    public function new(Request $request): Response
    {
        $transaction = new Transaction;
        $form = $this->createForm(TransactionFormType::class, $transaction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $transaction->setCreatedAt(new \DateTimeImmutable);
            $transaction->setUpdatedAt(new \DateTimeImmutable);
            $transaction->setUser($this->user);
            $transaction->setType($form->get('type')->getData());
            $transaction->setCategory($form->get('category')->getData());

            $this->entityManager->persist($transaction);
            $this->entityManager->flush();

            return $this->redirectToRoute('app_transactions');
        }

        return $this->renderForm('transactions/transaction_form.html.twig', [
            'title' => 'New Transaction',
            'user' => $this->user,
            'transaction_form' => $form
        ]);
    }

    #[Route('/transactions/update/{id}', name: 'app_transaction_update')]
    public function update($id, Request $request): Response
    {
        $transaction = $this->transactionRepository->find($id);

        if (!$transaction) {
            throw $this->createNotFoundException(
                'Unable to find transaction for id ' . $id
            );
        }

        $form = $this->createForm(TransactionFormType::class, $transaction);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $transaction->setUpdatedAt(new \DateTimeImmutable);
            $transaction->setType($form->get('type')->getData());
            $transaction->setCategory($form->get('category')->getData());

            $this->entityManager->persist($transaction);
            $this->entityManager->flush();

            return $this->redirectToRoute('app_transactions');
        }

        return $this->renderForm('transactions/transaction_form.html.twig', [
            'title' => 'Edit Transaction',
            'user' => $this->user,
            'transaction_form' => $form
        ]);
    }

    #[Route('/transactions/delete/{id}', name: 'app_transaction_delete')]
    public function delete($id, Request $request): Response
    {
        $transaction = $this->transactionRepository->find($id);

        if (!$transaction) {
            throw $this->createNotFoundException(
                'Unable to find transaction for id ' . $id
            );
        }

        $this->entityManager->remove($transaction);
        $this->entityManager->flush();

        return $this->redirectToRoute('app_transactions');
    }
}
