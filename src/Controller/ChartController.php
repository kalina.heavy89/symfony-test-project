<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\TransactionRepository;
use App\Service\TransactionService;
use App\Service\ChartService;
use App\Interfaces\CalendarDefinitions;
use App\Traits\CalendarTrait;

class ChartController extends AbstractController implements CalendarDefinitions
{
    use CalendarTrait;

    private $security;
    private $user;
    private $transactionRepository;
    private $transactionService;
    private $chartService;

    public function __construct(
        Security $security,
        TransactionRepository $transactionRepository,
        TransactionService $transactionService,
        ChartService $chartService
    )
    {
        $this->security = $security;
        $this->user = $this->security->getUser();
        $this->transactionRepository = $transactionRepository;
        $this->transactionService = $transactionService;
        $this->chartService = $chartService;
    }

    #[Route('/chart', name: 'app_chart')]
    public function index(Request $request): Response
    {
        $yearsOffset = 5;
        $filters = $this->transactionService->getFilters($request);
        $months = $this->getMonths(self::MONTHS);
        $years = $this->getYearsRange($filters['year'], $yearsOffset);

        $transactionsPerMonth = $this->transactionRepository->getTransactions($this->user, [
            'month' => $filters['month'],
            'year' => $filters['year'],
        ], 'ASC');

        $transactionsChartData = $this->chartService->getTransactionsChartData($transactionsPerMonth);
        $transactionDays = $transactionsChartData['days'];
        $transactionAmounts = $transactionsChartData['amounts'];
        $splittedTransactionAmounts = $this->chartService->splitIntoIncomesAndExpenses($transactionAmounts);

        $monthName = $this->getMonthName(self::MONTHS, $filters['month']);
        $chartBarTitle = 'Transaction Amounts for ' . $monthName;
        $chartBar = $this->chartService->createChartBar(
            $transactionDays,
            $splittedTransactionAmounts['incomes'],
            $splittedTransactionAmounts['expenses'],
            $chartBarTitle
        );

        $moneyBalanceData = $this->chartService->getMoneyBalanceData($filters['year'], $filters['month'], $transactionAmounts, $transactionDays);
        $moneyBalanceDays = $moneyBalanceData['days'];
        $moneyBalance = $moneyBalanceData['balance'];

        $chartLineTitle = 'Money Balance for ' . $monthName;
        $chartLine = $this->chartService->createChartLine($moneyBalanceDays, $moneyBalance, $chartLineTitle);

        return $this->render('chart/transaction_charts.html.twig', [
            'user' => $this->user,
            'months' => $months,
            'years' => $years,
            'filters' => $filters,
            'chart_bar' => $chartBar,
            'chart_line' => $chartLine,
        ]);
    }
}
