<?php

namespace App\Repository;

use App\Entity\Transaction;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Transaction>
 *
 * @method Transaction|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transaction|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transaction[]    findAll()
 * @method Transaction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransactionRepository extends ServiceEntityRepository
{
    public const PAGINATOR_PER_PAGE = 5;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transaction::class);
    }

    public function add(Transaction $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Transaction $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    private function initTransactionsQuery(User $user, array $filters)
    {
        $query = $this->createQueryBuilder('t')
            ->where('t.user = :user')
            ->setParameter('user', $user->getId());

        if (array_key_exists('search', $filters)) {
            $query->andWhere('t.comment LIKE :search')
                ->setParameter('search', '%' . $filters['search'] . '%');
        }

        if (array_key_exists('type', $filters) && $filters['type'] > 0) {
            $query->andWhere('t.type = :type')
                ->setParameter('type', $filters['type']);
        }

        if (array_key_exists('category', $filters) && $filters['category'] > 0) {
            $query->andWhere('t.category = :category')
                ->setParameter('category', $filters['category']);
        }

        if (array_key_exists('month', $filters)) {
            $query->andWhere('t.executionDate LIKE :month')
                ->setParameter('month', '%-' . $filters['month'] . '-%');
        }

        if (array_key_exists('year', $filters)) {
            $query->andWhere('t.executionDate LIKE :year')
                ->setParameter('year', $filters['year'] . '-%');
        }

        return $query;
    }

    public function countTransactions(User $user, array $filters): int
    {
        return count(
            $this->initTransactionsQuery($user, $filters)
                ->getQuery()
                ->getResult()
        );
    }

    public function getTransactions(User $user, array $filters, string $order = 'ASC', int $page = 0): array
    {
        $query = $this->initTransactionsQuery($user, $filters)
            ->orderBy('t.executionDate', $order);

            if ($page > 0) {
                $query->setFirstResult(($page - 1) * self::PAGINATOR_PER_PAGE)
                    ->setMaxResults(self::PAGINATOR_PER_PAGE);
            }

            return $query->getQuery()
                ->getResult();
    }

    public function getPreviousTransactions(User $user, $endDate): array
    {
        return $this->createQueryBuilder('t')
            ->where('t.user = :user')
            ->andWhere('t.executionDate < :endDate')
            ->setParameter('user', $user->getId())
            ->setParameter('endDate', $endDate)
            ->orderBy('t.executionDate', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
